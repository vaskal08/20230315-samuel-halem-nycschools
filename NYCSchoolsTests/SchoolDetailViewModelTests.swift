import XCTest
@testable import NYCSchools

class SchoolDetailViewModelTests: XCTestCase {
    
    var viewModel: SchoolDetailViewModel!
    
    override func setUp() {
        super.setUp()
        let school = School(dbn: "21K728",
                             schoolName: "William E. Grady Career and Technical Education High School",
                             boro: "Brooklyn",
                             overviewParagraph: "William E. Grady CTE High School provides students with an opportunity to explore career pathways in a variety of fields such as nursing, electrical engineering, culinary arts, and law and public safety.",
                             school10thSeats: "",
                             academicOpportunities1: "",
                             academicOpportunities2: "",
                             academicOpportunities3: "",
                             academicOpportunities4: "",
                             academicOpportunities5: "",
                             ellPrograms: "",
                             languageClasses: "",
                             diplomaEndorsements: "",
                             neighborhood: "Brighton Beach",
                             sharedSpace: "",
                             campusName: "William E. Grady Career and Technical Education High School",
                             buildingCode: "K443",
                             location: "25 Brighton 4th Road, Brooklyn NY 11235",
                             phoneNumber: "718-332-5000",
                             faxNumber: "718-648-1769",
                             schoolEmail: "gradyhighschool@yahoo.com",
                             website: "www.gradyhs.com",
                             subway: "B, Q to Brighton Beach; F to Neptune Avenue",
                             bus: "B1, B36, B68",
                             grades2018: "9-12",
                             finalGrades: "",
                             totalStudents: "734",
                             startTime: "8:20 AM",
                             endTime: "2:40 PM",
                             addtlInfo1: "",
                             extracurricularActivities: "Anime Club, Art Club, Chess Club, CUNY College Now, Dance Club, National Honor Society, Robotics Club, Yearbook Club",
                             psalSportsBoys: "Baseball, Basketball, Bowling, Football, Handball, Soccer, Tennis, Track & Field, Volleyball",
                             psalSportsGirls: "Basketball, Bowling, Flag Football, Handball, Soccer, Tennis, Track & Field, Volleyball",
                             schoolSports: "",
                             graduationRate: "83%",
                             attendanceRate: "89%",
                             pctStuEnoughVariety: "",
                             collegeCareerRate: "65%",
                             pctStuSafe: "",
                             pbat: "",
                             international: "",
                             schoolAccessibilityDescription: "",
                             program1: "Electrical Installation and Maintenance",
                             code1: "Z49E",
                             interest1: "Electrical Engineering",
                             method1: "",
                             seats9ge1: "15",
                             grade9gefilledflag1: "Y",
                             grade9geapplicants1: "124",
                             seats9swd1: "3",
                             grade9swdfilledflag1: "Y",
                             grade9swdapplicants1: "30",
                             seats101: "",
                             eligibility1: "",
                             grade9geapplicantsperseat1: "8",
                             grade9swdapplicantsperseat1: "10",
                             primaryAddressLine1: "25 Brighton 4th Road",
                             city: "Brooklyn",
                             zip: "11235",
                             stateCode: "NY",
                             latitude: "40.5794",
                             longitude: "-73.9609",
                             communityBoard: "13",
                             councilDistrict: "48",
                             censusTract: "373",
                             bin: "3189999",
                             bbl: "3067850021",
                             nta: "Brighton Beach-Coney Island",
                             borough: "Brooklyn")



        viewModel = SchoolDetailViewModel(school: school, dbn: "insert valid dbn here")
    }
    
    override func tearDown() {
        viewModel = nil
        super.tearDown()
    }
    
    func test_fetchSchoolData() {
        // Given
        let expectation = XCTestExpectation(description: "Fetching individual school data")
        
        // When
        viewModel.fetchSchoolData(dbn: viewModel.school.dbn)
        
        // Then
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            XCTAssertNotNil(self.viewModel.score, "Score should not be nil after successful API call")
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 10.0)
    }
}
