import Foundation
import SwiftUI

struct SchoolDetailView: View {
    @ObservedObject var viewModel: SchoolDetailViewModel
    
    var body: some View {
            ScrollView {
                VStack(alignment: .leading, spacing: 10) {
                    SingleLabelCardView(label: viewModel.school.schoolName, size: 26)
                    Text("OVERVIEW")
                        .font(.system(size: 20, weight: .bold))
                        .padding(.top, 20)
                   
                        SingleLabelCardView(label: viewModel.school.overviewParagraph, size: 17)

                    Spacer()
                    if let score = viewModel.score {
                        Text("Number of SAT Test Takers: \(score.numOfSatTestTakers)")
                        Text("SAT Critical Reading Avg Score: \(score.satCriticalReadingAvgScore)")
                        Text("SAT Math Avg Score: \(score.satMathAvgScore)")
                        Text("SAT Writing Avg Score: \(score.satWritingAvgScore)")
                    } else {
                        Text("No SAT Data Available")
                    }
                }
            }
            .padding()
    }
}
