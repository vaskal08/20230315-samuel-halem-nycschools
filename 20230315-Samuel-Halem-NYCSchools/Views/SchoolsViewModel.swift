import Foundation
import SwiftUI

class SchoolsViewModel: ObservableObject {
    private let api = SchoolsAPI()
    @Published var schools: [School] = []
    
    func fetchAllSchools() {
        api.fetchAllSchools { result in
            DispatchQueue.main.async {
                switch result {
                case .success(let schools):
                    self.schools = schools
                case .failure(let error):
                    // handle error
                    print(error)
                }
            }
        }
    }
}
