import Foundation
import SwiftUI

class SchoolDetailViewModel: ObservableObject {
    private let api = SchoolsAPI()
    @Published var score: SATScores?
    @Published var school: School
    
    init(school: School, dbn: String) {
        self.school = school
        fetchSchoolData(dbn: dbn)
    }

    func fetchSchoolData(dbn: String) {
        api.fetchIndividualSchoolData(forDBN: dbn) { result in
            switch result {
            case .success(let scoreArray):
                DispatchQueue.main.async {
                    let score = scoreArray.first
                    self.score = score
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
}
