import SwiftUI
import Foundation

struct SchoolsView: View {
    @ObservedObject var viewModel = SchoolsViewModel()
    
    var body: some View {
        NavigationView {
            List(viewModel.schools, id: \.dbn) { school in
                NavigationLink(destination: SchoolDetailView(viewModel: SchoolDetailViewModel(school: school, dbn: school.dbn))) {
                        CardView(title: school.schoolName,
                                 subtitle: school.borough ?? "No borough available")
                }
            }
            .navigationTitle("Schools")
            .onAppear {
                viewModel.fetchAllSchools()
            }
        }
    }
}
