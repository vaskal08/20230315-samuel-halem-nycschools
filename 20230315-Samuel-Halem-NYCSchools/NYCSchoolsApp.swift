//
//  NYCSchoolsApp.swift
//  NYCSchools
//
//  Created by Halem, Sam on 3/14/23.
//

import SwiftUI

@main
struct NYCSchoolsApp: App {
    var body: some Scene {
        WindowGroup {
            SchoolsView()
        }
    }
}
