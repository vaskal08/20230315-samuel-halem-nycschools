import Foundation

struct School: Codable, Identifiable {
    let dbn: String
    let schoolName: String
    let boro: String
    let overviewParagraph: String
    let school10thSeats: String?
    let academicOpportunities1: String?
    let academicOpportunities2: String?
    let academicOpportunities3: String?
    let academicOpportunities4: String?
    let academicOpportunities5: String?
    let ellPrograms: String?
    let languageClasses: String?
    let diplomaEndorsements: String?
    let neighborhood: String?
    let sharedSpace: String?
    let campusName: String?
    let buildingCode: String?
    let location: String?
    let phoneNumber: String?
    let faxNumber: String?
    let schoolEmail: String?
    let website: String?
    let subway: String?
    let bus: String?
    let grades2018: String?
    let finalGrades: String?
    let totalStudents: String?
    let startTime: String?
    let endTime: String?
    let addtlInfo1: String?
    let extracurricularActivities: String?
    let psalSportsBoys: String?
    let psalSportsGirls: String?
    let schoolSports: String?
    let graduationRate: String?
    let attendanceRate: String?
    let pctStuEnoughVariety: String?
    let collegeCareerRate: String?
    let pctStuSafe: String?
    let pbat: String?
    let international: String?
    let schoolAccessibilityDescription: String?
    let program1: String?
    let code1: String?
    let interest1: String?
    let method1: String?
    let seats9ge1: String?
    let grade9gefilledflag1: String?
    let grade9geapplicants1: String?
    let seats9swd1: String?
    let grade9swdfilledflag1: String?
    let grade9swdapplicants1: String?
    let seats101: String?
    let eligibility1: String?
    let grade9geapplicantsperseat1: String?
    let grade9swdapplicantsperseat1: String?
    let primaryAddressLine1: String?
    let city: String?
    let zip: String?
    let stateCode: String?
    let latitude: String?
    let longitude: String?
    let communityBoard: String?
    let councilDistrict: String?
    let censusTract: String?
    let bin: String?
    let bbl: String?
    let nta: String?
    let borough: String?
    
    var id: String { dbn }
    
    private enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case boro
        case overviewParagraph = "overview_paragraph"
        case school10thSeats = "school_10th_seats"
        case academicOpportunities1 = "academicopportunities1"
        case academicOpportunities2 = "academicopportunities2"
        case academicOpportunities3 = "academicopportunities3"
        case academicOpportunities4 = "academicopportunities4"
        case academicOpportunities5 = "academicopportunities5"
        case ellPrograms = "ell_programs"
        case languageClasses = "language_classes"
        case diplomaEndorsements = "diplomaendorsements"
        case neighborhood
        case sharedSpace = "shared_space"
        case campusName = "campus_name"
        case buildingCode = "building_code"
        case location
        case phoneNumber = "phone_number"
        case faxNumber = "fax_number"
        case schoolEmail = "school_email"
        case website
        case subway
        case bus
        case grades2018 = "grades2018"
        case finalGrades = "finalgrades"
        case totalStudents = "total_students"
        case startTime = "start_time"
        case endTime = "end_time"
        case addtlInfo1 = "addtl_info1"
        case extracurricularActivities = "extracurricular_activities"
        case psalSportsBoys = "psal_sports_boys"
        case psalSportsGirls = "psal_sports_girls"
        case schoolSports = "school_sports"
        case graduationRate = "graduation_rate"
        case attendanceRate = "attendance_rate"
        case pctStuEnoughVariety = "pct_stu_enough_variety"
        case collegeCareerRate = "college_career_rate"
        case pctStuSafe = "pct_stu_safe"
        case pbat
        case international
        case schoolAccessibilityDescription = "school_accessibility_description"
        case program1
        case code1
        case interest1
        case method1
        case seats9ge1
        case grade9gefilledflag1
        case grade9geapplicants1
        case seats9swd1
        case grade9swdfilledflag1
        case grade9swdapplicants1
        case seats101
        case eligibility1
        case grade9geapplicantsperseat1
        case grade9swdapplicantsperseat1
        case primaryAddressLine1 = "primary_address_line_1"
        case city
        case zip
        case stateCode = "state_code"
        case latitude
        case longitude
        case communityBoard = "community_board"
        case councilDistrict = "council_district"
        case censusTract = "census_tract"
        case bin
        case bbl
        case nta
        case borough
    }
}
