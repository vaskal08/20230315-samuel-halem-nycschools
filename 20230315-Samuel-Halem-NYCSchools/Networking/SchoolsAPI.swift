//
//  NetworkManager.swift
//  NYCSchools
//
//  Created by Halem, Sam on 3/14/23.
//

import Foundation

class SchoolsAPI {
    
    func fetchAllSchools(completion: @escaping (Result<[School], Error>) -> Void) {
        guard let url = URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json") else {
            let error = NSError(domain: "Invalid URL", code: 0, userInfo: nil)
            completion(.failure(error))
            return
        }
        
        let session = URLSession.shared
        let task = session.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil else {
                completion(.failure(error!))
                return
            }
            
            do {
                let schools = try JSONDecoder().decode([School].self, from: data)
                completion(.success(schools))
            } catch {
                completion(.failure(error))
            }
        }
        
        task.resume()
    }

    
    func fetchIndividualSchoolData(forDBN dbn: String, completion: @escaping (Result<[SATScores], Error>) -> Void) {
        let urlStr = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn=\(dbn)"
        guard let url = URL(string: urlStr) else {
            completion(.failure(NetworkError.invalidURL))
            return
        }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            guard let httpResponse = response as? HTTPURLResponse, (200...299).contains(httpResponse.statusCode) else {
                completion(.failure(NetworkError.invalidResponse))
                return
            }
            
            guard let data = data else {
                completion(.failure(NetworkError.invalidData))
                return
            }
            
            do {
                let schoolDetail = try JSONDecoder().decode([SATScores].self, from: data)
                completion(.success(schoolDetail))
            } catch {
                completion(.failure(error))
            }
        }.resume()
    }

}

enum NetworkError: Error {
    case invalidURL
    case invalidResponse
    case invalidData
    case decodingError
    case serverError
}
